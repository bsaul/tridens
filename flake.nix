{
  description = "tridens";
  nixConfig = {
    bash-prompt = "λ ";
  };
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.05";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system: let
      pkgs = nixpkgs.legacyPackages.${system};

      agda = pkgs.agda.withPackages (ps: [ ps.standard-library ]);
    in {

      packages.paper = pkgs.stdenv.mkDerivation {
        name = "tridens";
        version = "";
        src = ./.;
        buildInputs = [agda pkgs.pandoc];
        buildPhase = ''
          ${agda}/bin/agda --html --html-highlight=code tridens.lagda.md

          ${pkgs.pandoc}/bin/pandoc html/tridens.md \
             -o html/tridens.html \
             --css=html/Agda.css \
             --standalone

        '';
        installPhase = ''
          mkdir -p $out
          cp html/* $out/
        '';

      };


      devShells.default =  pkgs.mkShell {
        buildInputs = [

          agda
          # Documentation/writing tools
          pkgs.pandoc
        ];
      }; 
    });
}
