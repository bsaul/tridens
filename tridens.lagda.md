# Tridens


<details>
<summary>Show imports</summary>
```agda
open import Data.Maybe
open import Data.Empty using (⊥)
open import Data.Fin using (Fin)
open import Data.Fin.Patterns
open import Data.Nat using (ℕ)
open import Data.Unit using (⊤ ; tt)
open import Data.Product
open import Level using (0ℓ)
open import Relation.Binary using (Rel)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)
open import Relation.Nullary using (¬_)
```
</details>

## Experimental Setup

### Pathogens and their diseases

There are two pathogens being studied in these experiments:

```agda
data Pathogen : Set where
  -- Rhizoctonia solani
  -- https://en.wikipedia.org/wiki/Rhizoctonia
  Rhiz : Pathogen
  
  -- Colletotrichum
  -- https://en.wikipedia.org/wiki/Colletotrichum
  Coll : Pathogen
```

Each pathogen is associated with a disease,
at least in the host under study.
Hence, we simply use a synonym the names of the diseases.

```agda
BrownPatch = Rhiz
Anthracnose = Coll
```

### Experimental Treatments

The study has two main experimental manipulations, 
though the study will be repeated for multiple cohorts in a growing season
(see section on Cohorts).

```agda
data Fungicide : Set where
  Sprayed Unsprayed : Fungicide

data Inoculation : Pathogen → Set where 
  None : {x : Pathogen} → Inoculation x
  Inoculated : {x : Pathogen} → Inoculation x

Assignment = Inoculation Rhiz × Fungicide

-- treatment assignments
_ : Assignment; _ = None , Unsprayed
_ : Assignment ; _ = None , Sprayed
_ : Assignment ; _ = Inoculated , Unsprayed 
_ : Assignment ; _ = Inoculated , Sprayed
```

### Outcomes

```agda
postulate 
  ℝ⁺ : Set
  _<_ : Rel ℝ⁺ 0ℓ
  T30 : ℝ⁺
  T35 : ℝ⁺
  T30<T35 : T30 < T35

data Outcome : Pathogen → Set where
  Infected : {x : Pathogen} → ℝ⁺ → Outcome x
  Dead : {x : Pathogen} → ℝ⁺ → Outcome x

data Event : Pathogen → Set where
  O : {x : Pathogen} → Outcome x → Event x
  C : {x : Pathogen} → ℝ⁺ → Event x

Y = 
  Σ (Event BrownPatch × Event Anthracnose) 
  λ { (O (Infected _)  , O (Infected _))  → ⊤
    ; (O (Dead t₁)     , O (Dead t₂))     → t₁ ≡ t₂
    ; (C t₁            , C t₂ )           → t₁ ≡ t₂
    ; (O (Infected t₁) , O (Dead t₂))     → t₁ < t₂
    ; (O (Infected t₁) , C t₂)            → t₁ < t₂ 
    ; (O (Dead t₁)     , O (Infected t₂)) → t₂ < t₁
    ; (C t₁            , O (Infected t₂)) → t₂ < t₁
    ; (O (Dead _)      , C _)             → ⊥
    ; (C _             , O (Dead _))      → ⊥
    }

```

### Plants, Tillers, Leaves

```agda
Tiller : ℕ → Set 
Tiller m = Fin m → Y

exTiller₁ : Tiller 2 
exTiller₁ 0F = (O (Infected T30) , O (Infected T35)) , tt
exTiller₁ 1F = (O (Infected T30) , O (Dead T35)) , T30<T35

exTiller₂ : Tiller 3
exTiller₂ 0F = ((O (Dead T30)) , (O (Dead T30))) , refl
exTiller₂ 1F = ((O (Dead T30)) , (O (Dead T30))) , refl
exTiller₂ 2F = ((O (Dead T30)) , (O (Dead T30))) , refl

Plant : ℕ → Set
Plant n = Fin n → ∃ Tiller

exPlant : Plant 2 
exPlant 0F = 2 , exTiller₁
exPlant 1F = 3 , exTiller₂
```

### Cohorts

The experiment will be repeated five times each year,
every five weeks mid-April to early October.

```agda
Cohort = Fin 5
```
